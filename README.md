# Prueba Front-End

## Descripción
El objetivo de esta prueba es construir una aplicación web responsiva parecida a instagram pero utilizando imagenes de gatitos.

## Instrucciones:

* Construir una pantalla para listar a los gatitos.
* Construir una pantalla para listar a los gatitos marcados como favoritos.
* Agregar la opción para poder marcar la imagen de un gatito como favorito.
* Agregar la opción para poder eliminar la imagen de un gatito del listado de favoritos.

## Recursos:
Los recursos para realizar esta prueba provienen de https://docs.thecatapi.com/. Así mismo puedes utilizar la siguiente *API-KEY:* ***0deb10af-017e-4cb1-b4a8-c3d08d691587*** para hacer las peticiones.

> **NOTA:** Es importante agregar los encabezados a cada petición de la sigueinte manera:

Set-Headers:
* **x-api-key:** 0deb10af-017e-4cb1-b4a8-c3d08d691587
* **Content-Type:** application/json

## Listado de imágenes:

Las imágenes deben poder ser listadas utilizando los siguientes recursos:

### Listado de gatitos:
> **https://api.thecatapi.com/v1/images/search?limit={number}** 

![lista-gatitos](images/listado-gatos.PNG)

### Listado de gatitos favoritos:
> **https://api.thecatapi.com/v1/favourites?limit={number}**

![listado-gatos-favoritos](images/listado-favoritos.PNG)

## Opciones para marcar y eliminar una imagen como favorita:

Las imagenes deben poder ser marcadas como favoritas o eliminadas de mis favoritos, puedes utilizar los siguientes recursos:

### Marcar una imagen como favorita:

> **POST: **
> **https://api.thecatapi.com/v1/favourites** 
 
![marcar-favorito](images/make_as_favorite.PNG)

### Desmarcar una imagen como favorita:

> **DELETE: **
> **https://api.thecatapi.com/v1/favourites/{{favourite_id}}**

## Nota
> Como referencia puedes ver el siguiente DEMO: **https://laughing-swartz-acb879.netlify.app/home**

# ¡¡¡Mucho éxito!!!